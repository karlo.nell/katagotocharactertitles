console.log(correctTitle("jOn SnoW, kINg IN thE noRth"))
console.log(correctTitle("sansa stark,lady of winterfell."))
console.log(correctTitle("TYRION LANNISTER, HAND OF THE QUEEN."))

function correctTitle(str) {
    let corrected = [];
    let strs = [];

    if(str.includes(',') && str[str.indexOf(',') + 1] != ' ') {
        corrected = str.replace(',', ', ');
         strs = corrected.split(' ');
    }
    else strs = str.split(' ');
    
    let newStrs = [];

    strs.forEach(function (x) {
        let y = x.toLowerCase().trim();

        if (y.includes(',')) {
            y[y.indexOf(',')].concat(' ');
        }
        //syncategorematic words to lower
        if (y == "and" || y == "the" || y == "of" || y == "in") {
            y.toLowerCase();
        }
        //else make first char Capitalized
        else {
            let word = y;
            y = word[0].toUpperCase() + word.slice(1).toLowerCase();
            
        }
        newStrs.push(y);
    })

    let joined = newStrs.join(' ');

    if (!(joined.indexOf('.') == joined.length -1)) {
        joined = joined.concat('.');
    }
    return joined;

}